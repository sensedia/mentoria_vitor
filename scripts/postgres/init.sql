CREATE DATABASE skeleton
    WITH
    OWNER = postgres;

\connect skeleton;

CREATE SCHEMA skeletonschema;
