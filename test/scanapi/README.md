# ScanAPI - Automated Integration Testing

 Learn more about [ScanAPI](https://scanapi.dev/).
 
Requirements:
-----
```shell
$ pip install -r requirements.txt
```


### First steps:

Duplicate .env.example and rename it to .env and configure your keys. 

:warning: Do not commit .env file

apply the enviroment:
```
$ source .env
```

## Running ScanAPI

```
$ scanapi run scanapi.yaml
```
If you want to generate the report in a custom template:
```
$ scanapi run -t templates/custom-report.html scanapi.yaml
```

The report will be available at `skeleton-test-report.html`
