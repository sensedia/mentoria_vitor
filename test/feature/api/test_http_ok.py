

def test_http_ok(base_path, web_session):
    response = web_session.get(f'{base_path}/')
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}
