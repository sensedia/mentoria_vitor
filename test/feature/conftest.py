import pytest

from nameko import config
from nameko.testing.utils import find_free_port

from httpConnector.service import HttpConnectorService


pytest_plugins = [
    "test.feature.fixtures.mongo_fixture",
    "test.feature.fixtures.postgres_fixture",
    "test.feature.fixtures.s3_fixture",
]


@pytest.fixture
def test_config(rabbit_config, db_url):
    with config.patch({

    }):
        yield


@pytest.fixture
def base_path():
    ms_name = 'skeleton'
    env = 'dev'
    api_version = 'v1'
    return f'/{ms_name}-{env}/{api_version}'


@pytest.fixture
def web_session(rabbit_config, container_factory,
                web_config, web_session):
    container = container_factory(HttpConnectorService, web_config)
    container.start()
    return web_session
