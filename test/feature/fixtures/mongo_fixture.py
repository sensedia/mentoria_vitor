import pytest

from nameko import config
from nameko.testing.utils import find_free_port
from nameko.testing.services import entrypoint_hook

from mongoConnector.service import MongoConnectorService


#####################
# CONTAINER STARTER #
#####################


@pytest.fixture
def mongo_connector_container(container_factory, test_config):
    with config.patch({
        'WEB_SERVER_ADDRESS': f'0.0.0.0:{find_free_port()}',
        'MONGODB_URL': 'mongodb://localhost:27017',
        'MONGODB_DATABASE': 'promocoesdb',
        'MONGODB_USER': 'admin',
        'MONGODB_PASSWORD': 'pr0m0c03s'
    }):
        container = container_factory(MongoConnectorService)
        container.start()
    return container



###############
# ENTRYPOINTS #
###############

