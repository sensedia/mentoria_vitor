import pytest

from nameko import config
from nameko.testing.utils import find_free_port
from nameko.testing.services import entrypoint_hook

from postgresConnector.service import PostgresConnectorService


#####################
# CONTAINER STARTER #
#####################


@pytest.fixture
def postgres_connector_container(container_factory, test_config):
    with config.patch({
        'WEB_SERVER_ADDRESS': f'0.0.0.0:{find_free_port()}',
        'DB_URIS': {
            "skeleton_postgresConnector_dev:Base":
                "postgresql://postgres:password@localhost:5432/skeleton"
        }
    }):
        container = container_factory(PostgresConnectorService)
        container.start()
    return container


###############
# ENTRYPOINTS #
###############


@pytest.fixture
def execute_write_rpc(postgres_connector_container):
    with entrypoint_hook(postgres_connector_container,
                         'execute_write') as entrypoint:
        return entrypoint


@pytest.fixture
def execute_read_rpc(postgres_connector_container):
    with entrypoint_hook(postgres_connector_container,
                         'execute_read') as entrypoint:
        return entrypoint
