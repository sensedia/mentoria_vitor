# Microsserviços usando Nameko 
![Airship Ltd](airship.png)

## Pré-requisitos
* [Python 3](https://www.python.org/downloads/)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

## Opicionais
* [PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux)
* [Anaconda](https://www.anaconda.com/distribution/)

## Overview

### Estrutura
Para o exemplo referência, foram construídos trẽs serviços:
`Products`, `Orders` and `Gateway`.

Gateway: É responsável por receber requisição HTTP e comunicar via RPC com os serviços Orders e Products, por sua vez, o papel é mais de um orquestrador.
[Marshmallow](https://pypi.python.org/pypi/marshmallow) é framework usado como validator de tipos. Serializando e deserializando objetos Json.

Orders: É responsável por receber requiições do serviço Gateway, processar as regras de negócio e salvar dados no PostgreSQL.
- [nameko-sqlalchemy](https://pypi.python.org/pypi/nameko-sqlalchemy)  ORM. 
- [Alembic](https://pypi.python.org/pypi/alembic) é uma ferramenta de migração de banco de dados para sqlalchemy. 

Products: É responsável por receber requisições do serviço Gateway, processar as regras de negócio e salvar dados no Redis.
Tem um exemplo de implementação de injeção de dependência [DependencyProvider](https://nameko.readthedocs.io/en/stable/key_concepts.html#dependency-injection)

### Services

![Serviços](diagram.png)


## Modos de rodar os exemplos
#### Docker
`$ docker-compose up` para subir todos os exemplos.  
`$ docker-compose up -d [container_name]` para subir algum contairner específico.

Os serviços gateway, orders e products irão ficar expostas na porta 8003.

Para testar os serviços rpc entre no container, no diretório do Nameko rode:  
`$ nameko shell`  
`$ n.rpc.class.method(parameter=value)`

#### Local
Garantir que o RabbitMQ, PostgreSQL e Redis estão rodando. 

`$ python path/to/setup.py install`  
`$ nameko --config config.yml path.to.service` 

Para testar os serviços rpc entre local execute os mesmos comandos citados no modo docker utilizando linha de comando.  

#### Criando Product
```sh
[HTTP][POST]
http://localhost:8003/products
{
    "id": "the_odyssey",
    "title": "The Odyssey",
    "passenger_capacity": 101, 
    "maximum_speed": 5, 
    "in_stock": 10
} 
```
#### Buscando Product
```sh
[HTTP][GET]
http://localhost:8003/products/the_odyssey'
{
    "id": "the_odyssey",
    "title": "The Odyssey",
    "passenger_capacity": 101,
    "maximum_speed": 5,
    "in_stock": 10
}
```
#### Criando Order
```sh
[HTTP][POST]
http://localhost:8003/orders
{
    "order_details": [{
        "product_id": "the_odyssey", 
        "price": "100000.99", 
        "quantity": 1
    }]
} 

Response
{"id": 1}
```
#### Buscando Order
```sh
[HTTP][GET]
http://localhost:8003/orders/1
{
    "id": 1,
    "order_details": [{
        "id": 1,
        "quantity": 1,
        "product_id": "the_odyssey",
        "image": "http://www.example.com/airship/images/the_odyssey.jpg",
        "price": "100000.99",
        "product": {
            "maximum_speed": 5,
            "id": "the_odyssey",
            "title": "The Odyssey",
            "passenger_capacity": 101,
            "in_stock": 9
        }
    }]
}
```

## Modo de rodar os testes
Garanta que o RabbitMQ, PostgreSQL e Redis estão rodando.

As configurações dos testes estão no path/to/test/conftest.py  
gateway -> with config.patch(
{'PRODUCT_IMAGE_ROOT': 'http://example.com/airship/images'}
): yield

orders -> with config.patch(
{'DB_URIS': {'orders:Base': db_url}}
): yield

products -> with config.patch(
        {REDIS_URI_KEY: 'redis://:your_password@localhost:{port}'}
): yield

Use Pytest rodar os testes:  
`$ python -m pytest test/path/to/file::testName -v`

Para ver a cobertura de testes e rodar todos os testes:  
`$ make coverage`
