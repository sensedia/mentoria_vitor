#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='postgres-connector',
    version='0.0.1',
    description='Postgres Connector',
    packages=find_packages(exclude=['test', 'test.*']),
    install_requires=[
        'dnspython==1.16.0',
        'nameko==v3.0.0-rc9',
        'py-healthcheck==1.10.1',
        'nameko-sqlalchemy==1.5.0',
        'alembic==1.0.10',
        'psycopg2-binary==2.8.5',
    ],
    extras_require={
        'dev': [
            'pytest==4.5.0',
            'coverage==4.5.3',
            'flake8==3.7.7'
        ]
    },
    zip_safe=True
)
