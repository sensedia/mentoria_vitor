#!/bin/bash
#set -x

IMAGE_NAME=""
MS_NAME=""
VERSAO="v3"
CONTAINER_EXEC="docker"

echo "0: $0"
echo "1: $1"

if [ "$1" ] ; then
 CONTAINER_EXEC="$1"
 echo  "CONTAINER_EXEC : $CONTAINER_EXEC"
fi

 aws ecr get-login-password --region us-east-1 |  bash -c " $CONTAINER_EXEC login --username AWS --password-stdin 670663451679.dkr.ecr.us-east-1.amazonaws.com/microservicos/$MS_NAME/$IMAGE_NAME "

 bash -c " $CONTAINER_EXEC build -t microservicos/$MS_NAME/$IMAGE_NAME:$VERSAO . "


 bash -c "$CONTAINER_EXEC tag microservicos/$MS_NAME/$IMAGE_NAME:$VERSAO 670663451679.dkr.ecr.us-east-1.amazonaws.com/microservicos/$MS_NAME/$IMAGE_NAME:$VERSAO "
 bash -c "$CONTAINER_EXEC push 670663451679.dkr.ecr.us-east-1.amazonaws.com/microservicos/$MS_NAME/$IMAGE_NAME:$VERSAO "

 bash -c "$CONTAINER_EXEC tag microservicos/$MS_NAME/$IMAGE_NAME:$VERSAO 670663451679.dkr.ecr.us-east-1.amazonaws.com/microservicos/$MS_NAME/$IMAGE_NAME:latest "

 bash -c "$CONTAINER_EXEC push 670663451679.dkr.ecr.us-east-1.amazonaws.com/microservicos/$MS_NAME/$IMAGE_NAME:latest "
