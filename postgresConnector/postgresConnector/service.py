import logging
import json
import os

from nameko.rpc import RpcProxy, rpc
from nameko.web.handlers import http
from nameko_sqlalchemy import DatabaseSession
from sqlalchemy.ext.declarative import declarative_base
from werkzeug import Response


def get_ms_name():
    return os.getenv('MICROSERVICE_NAME') or 'skeleton'


def get_ms_env():
    return os.getenv('MICROSERVICE_ENVIRONMENT') or 'dev'


class PostgresConnectorService(object):
    log = logging.getLogger(__name__)
    name = f'{get_ms_name()}_postgresConnector_{get_ms_env()}'

    db = DatabaseSession(declarative_base())

    @http("GET", "/health")
    def get_health(self, request):
        return Response(
            json.dumps({'msg': 'ok'}), status=200, mimetype="application/json"
        )

    @rpc
    def home(self):
        return json.dumps({'message': 'Ok'})

    @rpc
    def skeleton_postgres(self):
        return {'message': 'Postgres response OK!'}

    @rpc
    def execute_read(self, sql):
        self.log.info('postgresConnector.execute_read:: start')
        self.log.info('postgresConnector.execute_read:: SQL {}'.format(sql))
        result = self.db.execute(sql)
        result_fetched = self._fetch_query_result(result)
        self.log.info('postgresConnector.execute_read:: end')
        return result_fetched

    @rpc
    def execute_write(self, sql):
        self.log.info('postgresConnector.execute_write:: start')
        self.log.info('postgresConnector.execute_write:: SQL: {}'.format(sql))
        self.db.execute(sql)
        self.db.commit()
        self.log.info('postgresConnector.execute_write:: end')

    def _fetch_query_result(self, result):
        self.log.info('postgresConnector._fetch_query_result: start')
        self.log.info('postgresConnector._fetch_query_result: result fetched ')
        object_fetched, result_fetched = {}, []
        # each row is an object in database
        for row_proxy in result:
            # row_proxy.items() returns an array like [(key0, value0), (key1, value1)]
            for column, value in row_proxy.items():
                # build up the dictionary
                object_fetched = {**object_fetched, **{column: value}}
            result_fetched.append(object_fetched)

        self.log.info(f'postgresConnector._fetch_query_result: result fetched {result_fetched}')
        self.log.info('postgresConnector._fetch_query_result: end')
        return result_fetched

