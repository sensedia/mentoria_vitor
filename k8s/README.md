# Run Nameko Examples on Kubernetes

![Nameko loves Kubernetes](nameko-k8s.png)

In this example we'll use local Kubernetes cluster installed with [docker-for-desktop](https://docs.docker.com/docker-for-mac/)
 along with community maintained Helm Charts to deploy all 3rd party services. 
 We will also create a set of Helm Charts for Nameko Example Services from this repository.  

Tested with Kubernetes v1.14.8

## Prerequisites

Please make sure these are installed and working

* [docker-for-desktop](https://docs.docker.com/docker-for-mac/) with Kubernetes enabled. Docker Desktop for Mac is used in these examples but any other Kubernetes cluster will work as well.
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) - should be installed and configured during docker-for-desktop installation
* [Helm](https://docs.helm.sh/using_helm/#installing-helm)

Verify our Kubernetes cluster us up and running:

```sh
$ kubectl get nodes

NAME             STATUS   ROLES    AGE   VERSION
docker-desktop   Ready    master   54m   v1.14.8
```

## Create Namespace

It's a good practice to create a namespaces where all of our services will live:

```yaml
# namespace.yaml

apiVersion: v1
kind: Namespace
metadata:
  name: examples
```

```sh
$ kubectl apply -f namespace.yaml

namespace/examples created
```

## Install External Dependencies

Our examples depend on PostgreSQL, Redis and RabbitMQ. 
The fastest way to install these 3rd party dependencies is to use community maintained [Helm Charts](https://github.com/kubernetes/charts).

Let's verify that Helm client is installed

```sh
$ helm version 

version.BuildInfo{Version:"v3.0.0", GitCommit:"e29...8b6", GitTreeState:"clean", GoVersion:"go1.13.4"}
```

### Deploy RabbitMQ, PostgreSQL and Redis

Run these commands one by one:

```sh
# Depreciated
$ helm install broker stable/rabbitmq -n examples

$ helm install db --namespace examples stable/postgresql --set postgresqlDatabase=orders

$ helm install cache  --namespace examples stable/redis
```

Repositório "stable/ " está depreciado!
Novo repositório é o bitnami



```buildoutcfg
# Adicionar repo bitnami:
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm upgrade

# Listar todos as aplicações no repositório:
$ helm search repo bitnami

# Instalar Rabbit com o novo repo:
$ helm install broker bitnami/rabbitmq -n examples
```

Mais informações: https://github.com/bitnami/charts/

---

RabbitMQ, PostgreSQL and Redis are now installed along with persistent volumes, kubernetes services, config maps and any secrets required a.k.a. `Amazing™`!

Verify all pods are running:

```sh
$ kubectl get pods -n examples

NAME                               READY     STATUS    RESTARTS   AGE
broker-rabbitmq-0                   1/1       Running   0          49s
cache-redis-master-0                1/1       Running   0          49s
cache-redis-slave-79fc9cc57-s52gw   1/1       Running   0          49s
db-postgresql-0                     1/1       Running   0          49s
```

## Deploy Example Services

To deploy our example services, we'll have to create Kubernetes deployment definition files. 
Most of the time (in real world) you would want to use some dynamic data during your deployments e.g. define image tags.
The easiest way to do this is to create Helm Charts for each of our service and use Helm to deploy them. 

Our charts are organized as follows:

```txt
charts
├── http-connector
│   ├── Chart.yaml
│   ├── templates
│   │   ├── NOTES.txt
│   │   ├── deployment.yaml
│   │   ├── ingress.yaml
│   │   └── service.yaml
│   └── values.yaml
├── 
│   ├── Chart.yaml
│   ├── templates
│   │   ├── NOTES.txt
│   │   └── deployment.yaml
│   └── values.yaml
└── 
    ├── Chart.yaml
    ├── templates
    │   ├── NOTES.txt
    │   └── deployment.yaml
    └── values.yaml
```

Each chart is comprised of:

`Charts.yaml` file containing description of the chart.  
`values.yaml` file containing default values for a chart that can be overwritten during the release..  
`templates` folder where all Kubernetes definition files live.

All of our charts contain `deployment.yaml` template where main Nameko Service deployment definition lives. `http-connector` chart has additional definitions for `ingress` and kubernetes `service` which are required to enable inbound traffic.

Example of http-connector `deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Chart.Name }}
  labels:
    app: {{ .Chart.Name }}
    tag: {{ .Values.image.tag }}
    revision: "{{ .Release.Revision }}"
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ .Chart.Name }}
  template:
    metadata:
      labels:
        app: {{ .Chart.Name }}
    spec:
      containers:
      - name: {{ .Chart.Name }}
        image: http-connector:{{ .Values.image.tag }}
        imagePullPolicy: Never
        env:
          - name: MICROSERVICES_ENV
            value: {{ .Values.microsevicesEnv }}
          - name: RABBIT_HOST
            value: broker-rabbitmq
          - name: RABBIT_MANAGEMENT_PORT
            value: "15672"
          - name: RABBIT_PORT
            value: "5672"
          - name: RABBIT_USER
            value: user
          - name: RABBIT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: broker-rabbitmq
                key: rabbitmq-password
      restartPolicy: Always
```

As you can see this template is using values coming from `Chart` and `Values` files as well as dynamic `Release` information. Password from secrets created RabbitMQ releases are also referenced and passed to a container as `RABBIT_PASSWORD` environment variables respectively.

Please read [The Chart Template Developer’s Guide](https://docs.helm.sh/chart_template_guide/#the-chart-template-developer-s-guide)
to learn about creating your own charts.

To route traffic to our gateway service we'll be using ingress. For ingress to work `Ingress Controller` has to be enabled on our cluster. Follow instructions form [Ingress Installation docs](https://kubernetes.github.io/ingress-nginx/deploy/):

```sh
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml

$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml
```

Let's deploy our `http-connector` chart:

Run the command from k8s directory:

```sh
$ helm upgrade http-connector charts/httpConnector --install -n examples

Release "http-connector" does not exist. Installing it now.
NAME: http-connector
LAST DEPLOYED: Wed Mar 25 11:23:53 2020
NAMESPACE: examples
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Thank you for installing Http Connector Service!
```

We used `--set image.tag=latest` to set custom image tag to be used for this release. You can do the same for any values defined in values.yaml file.

Let's release others services:

```sh
$ helm upgrade [service name] charts/[service folder name] --install --set image.tag=latest -n examples
	
(...)
```

Let's list all of our Helm releases:

```sh
$ helm list -n examples

NAME    	REVISION	UPDATED                 	STATUS  	CHART
broker  	1       	Tue Oct 29 06:50:26 2019	DEPLOYED	rabbitmq-4.11.1
cache   	1       	Tue Oct 29 06:50:43 2019	DEPLOYED	redis-6.4.4
db      	1       	Tue Oct 29 06:50:35 2019	DEPLOYED	postgresql-3.16.1
http-connector 	1       	Tue Oct 29 06:54:09 2019	DEPLOYED	http-connector-0.1.0
```

And again let's verify pods are happily running:

```sh
$ kubectl get pods -n examples

NAME                                 READY   STATUS    RESTARTS   AGE
broker-rabbitmq-0                    1/1     Running   0          6m30s
cache-redis-master-0                 1/1     Running   0          6m13s
cache-redis-slave-0                  1/1     Running   0          6m13s
db-postgresql-0                      1/1     Running   0          6m20s
http-connector-556c4d66ff-g2fnf      1/1     Running   0          58m

```

## Run examples

We can now verify our gateway api is working as expected by executing sample requests found in main README of this repository.

Runing in minikube. Check your minikube IP:

```
$ minikube ip
192.168.99.100

$ curl 'http://192.168.99.100/'
{
  "status": "ok"
}
```

## Criação de secrets:

```
kubectl create secret generic creditoms-nome-do-secredo --from-literal=dbname='NomeDoBanco' --from-literal=user='NomedoUsuario' --from-literal=password='SenhadoBanco' -n microservices-nonprod
```
No `--from-literal=dbname='NomeDoBanco'`.
É criado uma chave com nome dbname (que está dentro do objecto secret chamado: creditoms-nome-do-segredo), que contém o valor do nome do banco de dados

Para ter acesso aos valores dentro do secredo, no deployment.yaml usamos o:

```
valueFrom:
  secretKeyRef:
    name: "creditoms-nome-do-secredo"
    key: "dbname"
```
O código irá buscar dentro do segredo creditoms-nome-do-secredo o valor que está dentro da chame dbname.

Dentro de um secret podemos adicionar várias chaves

## Wrap-up

Running Nameko services in Kubernetes is really simple. Please get familiar with Helm Charts included in this repository and try adding one of your own. 
