#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='service-connector-rpc',
    version='0.0.1',
    description='Service Connector',
    author='nameko',
    packages=find_packages(exclude=['test', 'test.*']),
    py_modules=['serviceConnector'],
    install_requires=[
        'dnspython==1.16.0',
        'nameko==v3.0.0-rc6',
        'nameko-structlog==0.1.1',
        'marshmallow==3.5.1',
        'zeep==3.4.0'
    ],
    extras_require={
        'dev': [
            'pytest==4.5.0',
            'coverage==4.5.3',
            'flake8==3.7.7',
            'Faker==4.0.2',
        ],
    },
    zip_safe=True
)
