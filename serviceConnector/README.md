# Serviço Service Connector 

Serviço usado para conectar web services .

#### Pré-requisitos
* [Python 3](https://www.python.org/downloads/)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

#### Opcionais
* [PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux)
* [Anaconda](https://www.anaconda.com/distribution/)

## Como buildar 
`$ python setup.py install`

## Como executar 
`$ nameko run --config config.yml crivo.service`
Observação: Certifique que o RabbitMQ esteja rodando! As configurações de conexão com o RabbitMQ encontra-se no arquivo `config.yml` pela tag `AMQP_URI`.
  
## Modo de rodar os testes 
`$ python -m pytest test/path/to/file::testName -v`