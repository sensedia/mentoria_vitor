from marshmallow import fields


class TrueOrFalseField(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is not None and type(value) == bool:
            if value:
                return 'S'

            if not value:
                return 'N'

        return ''

    def _deserialize(self, value, attr, data, **kwargs):
        return value.lower()