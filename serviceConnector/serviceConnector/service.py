import json
import os
import zeep

from nameko.rpc import rpc
from nameko_structlog import StructlogDependency

from .exceptions import WebServerError
from zeep import Client


def get_ms_name():
    return os.getenv('MICROSERVICE_NAME') or 'referencia_python'


def get_ms_env():
    return os.getenv('MICROSERVICE_ENVIRONMENT') or 'dev'


class ServiceConnector:
    # an example
    client = Client('http://www.webservicex.net/ConvertSpeed.asmx?WSDL')

    result = client.service.ConvertSpeed(
        100, 'kilometersPerhour', 'milesPerhour')

    assert result == 62.137
    pass
