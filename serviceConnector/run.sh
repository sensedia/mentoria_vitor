#!/bin/bash

# Check if rabbit is up and running before starting the service.

# shellcheck disable=SC2086
until nc -z ${RABBIT_HOST} ${RABBIT_PORT}; do
    echo "$(date) - waiting for rabbitmq..."
    sleep 2
done

# Run Service

nameko run --config config.yml serviceConnector.service --backdoor 3000
