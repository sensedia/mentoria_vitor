#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='s3-connector',
    version='0.0.1',
    description='S3 Connector',
    packages=find_packages(exclude=['test', 'test.*']),
    install_requires=[
        'dnspython==1.16.0',
        'nameko==v3.0.0-rc9',
        'py-healthcheck==1.10.1',
        'boto3==1.14.39',
        'retry==0.9.2'
    ],
    extras_require={
        'dev': [
            'pytest==4.5.0',
            'coverage==4.5.3',
            'flake8==3.7.7',
            'moto==1.3.14'
        ]
    },
    zip_safe=True
)
