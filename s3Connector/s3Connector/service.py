import os
import boto3
import json
import logging

from botocore.exceptions import ClientError
from nameko.rpc import rpc
from nameko.web.handlers import http
from s3Connector.exceptions import S3ConnectorError
from werkzeug import Response
from retry import retry


def get_ms_name():
    return os.getenv('MICROSERVICE_NAME') or 'skeleton'


def get_ms_env():
    return os.getenv('MICROSERVICE_ENVIRONMENT') or 'dev'


def get_bucket_name():
    return os.getenv('BUCKET_NAME') or 'rodbucket-skeleton-ms-dev'


class S3ConnectorService(object):
    logger = logging.getLogger('S3ConnectorService')
    name = f'{get_ms_name()}_s3Connector_{get_ms_env()}'
    logger.info(f'starting: {name}')

    @http('GET', '/v1/health')
    def get_health(self, request):
        return Response(
            json.dumps({'msg': 'ok'}), status=200, mimetype="application/json"
        )

    @rpc
    def save(self, key, payload):
        self.logger.info('save: start')
        self.logger.info(f'save: key:{key}')
        self.logger.info(f'save: payload:{payload}')

        bucket = self._get_bucket()

        self._save_data_in_s3(bucket, key, payload)
        self.logger.info('save: end')

    @rpc
    def recover(self, key):
        self.logger.info('recover: start')
        self.logger.info(f'recover: key:{key}')
        bucket = self._get_bucket()

        response = self._get_data_in_s3(bucket, key)
        self.logger.info('recover: end')
        return response

    def _get_bucket(self):
        self.logger.info('_get_bucket: start')
        bucket = boto3.resource('s3').Bucket(get_bucket_name())
        self.logger.info('_get_bucket: end')
        return bucket

    def _get_s3_resource(self):
        self.logger.info('_get_s3_resource: start')
        resource = boto3.resource('s3')
        self.logger.info('_get_s3_resource: end')
        return resource

    @retry(exceptions=Exception, tries=3, delay=2, logger=logger)
    def _save_data_in_s3(self, bucket, file_name, data):
        self.logger.info('_save_data_in_s3: start')
        self.logger.info(f'_save_data_in_s3: file_name:{file_name}')
        parsed_data = json.dumps(data)
        try:
            bucket.put_object(Key=file_name, Body=parsed_data)
        except ClientError as e:
            self.logger.error(f'_save_data_in_s3: data not saved in s3 bucket'
                              f' - {str(e)}')
            raise S3ConnectorError(str(e))
        self.logger.info('_save_data_in_s3: end')

    def _get_data_in_s3(self, file_name):
        self.logger.info('_get_data_in_s3: start')
        self.logger.info(f'_get_data_in_s3: file_name:{file_name}')
        try:
            s3 = self._get_s3_resource()
            obj = s3.Object(get_bucket_name(), file_name)
            response = obj.get()['Body'].read()
        except ClientError as e:
            self.logger.error(f'_get_data_in_s3: can not get data in s3 bucket'
                              f' - {str(e)}')
            raise S3ConnectorError(str(e))

        response = json.loads(response)
        self.logger.info(f'_get_data_in_s3: data:{response}')
        self.logger.info('_get_data_in_s3: end')
        return response
