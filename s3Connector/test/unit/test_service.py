import os
import datetime

import boto3
import pytest
from moto import mock_s3
from s3Connector.service import (S3ConnectorService,
                                 get_ms_name,
                                 get_ms_env,
                                 get_bucket_name)


@pytest.fixture
def service():
    return S3ConnectorService()


def test_get_ms_name():
    os.environ['MICROSERVICE_NAME'] = 'test_name'
    assert get_ms_name() == 'test_name'

    os.environ['MICROSERVICE_NAME'] = ''
    assert get_ms_name() == 'skeleton'


def test_get_ms_env():
    os.environ['MICROSERVICE_ENVIRONMENT'] = 'test_env'
    assert get_ms_env() == 'test_env'

    os.environ['MICROSERVICE_ENVIRONMENT'] = ''
    assert get_ms_env() == 'dev'


def test_get_bucket_name():
    os.environ['BUCKET_NAME'] = 'test_bucket'
    assert get_bucket_name() == 'test_bucket'

    os.environ['BUCKET_NAME'] = ''
    assert get_bucket_name() == 'rodbucket-skeleton-ms-dev'


@mock_s3
def test__save_data_in_s3(service):
    conn = boto3.resource('s3', region_name='us-east-1')
    bucket = conn.create_bucket(Bucket='my_bucket')
    event_name = 'TEST'
    datetime_now = datetime.datetime.now()
    signature = '640AB2BAE07BEDC4C163F679A746F7AB7FB5D1FA'
    data = '{}'

    service._save_data_in_s3(bucket=bucket,
                             event_name=event_name,
                             datetime_now=datetime_now,
                             signature=signature,
                             data=data)

    files = list(bucket.objects.filter(Prefix=f'{event_name}'))
    files_key = list(map(lambda file: file.key, files))
    assert f'{event_name}/{datetime_now}-{signature}.payload' in files_key
