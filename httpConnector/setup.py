#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='http-connector',
    version='0.0.1',
    description='Http Connector',
    packages=find_packages(exclude=['test', 'test.*']),
    install_requires=[
        "marshmallow==3.9.1",
        "nameko==v3.0.0-rc9",
        "py-healthcheck==1.9.0",
        'nameko-structlog==0.1.1',
    ],
    extras_require={
        'dev': [
            'pytest==4.5.0',
            'coverage==4.5.3',
            'flake8==3.7.7',
        ],
    },
    zip_safe=True
)
