import json

from marshmallow import ValidationError
from nameko.exceptions import safe_for_serialization, BadRequest
from nameko.web.handlers import HttpRequestHandler
from werkzeug import Response


class HttpEntrypoint(HttpRequestHandler):
    """ Overrides `response_from_exception` so we can customize error handling.
    """

    mapped_errors = {
        BadRequest: (400, 'BAD_REQUEST'),
    }

    def response_from_exception(self, exc):
        status_code, error_code = 500, 'UNEXPECTED_ERROR'

        if isinstance(exc, self.expected_exceptions):
            status_code = self.mapped_errors[type(exc)]

        message = 'Erro interno do servidor' if status_code == 500 else safe_for_serialization(exc)
        return Response(
            json.dumps({
                'mensagem': message
            }),
            status=status_code,
            mimetype='application/json'
        )


http = HttpEntrypoint.decorator
