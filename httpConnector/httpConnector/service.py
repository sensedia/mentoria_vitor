import json
import logging
import os

from nameko.exceptions import BadRequest
from .entrypoints import http
from nameko.rpc import RpcProxy
from werkzeug import Response


from healthcheck import HealthCheck


def get_ms_name():
    return os.getenv('MICROSERVICE_NAME') or 'skeleton'


def get_ms_env():
    return os.getenv('MICROSERVICE_ENVIRONMENT') or 'dev'


class HttpConnectorService(object):
    """
    Service acts as a gateway to other services over http.
    """

    api_version = 'v1'
    base_path = f'/{get_ms_name()}-{get_ms_env()}/{api_version}'

    log = logging.getLogger(__name__)
    name = f'{get_ms_name()}_httpConnector_{get_ms_env()}'
    postgres_rpc = RpcProxy(f'{get_ms_name()}_postgresConnector_{get_ms_env()}')

    @http("GET", f"{base_path}/", expected_exceptions=BadRequest)
    def home(self, request):
        self.log.info('httpConnector::home -> start')
        self.log.info('httpConnector::home -> end')
        return Response(
            json.dumps({'status': 'ok'}),
            mimetype='application/json'
        )

    @http("GET", f"{base_path}/postgres-rpc", expected_exceptions=BadRequest)
    def postgres_test(self, request):
        self.log.info('httpConnector::postgres_rpc -> start')

        response = self.postgres_rpc.skeleton_postgres()

        self.log.info('httpConnector::postgres_rpc -> end')
        return Response(
            json.dumps(response),
            mimetype='application/json'
        )

    @http("GET", "/health")
    def get_health(self, request):
        """Gets Health
        """
        health = HealthCheck()
        health.add_check(self._get_health)
        content, status, type = health.run()
        return Response(
            content,
            status=status,
            mimetype=type['Content-Type']
        )

    def _get_health(self):
        return True, 'ok'
